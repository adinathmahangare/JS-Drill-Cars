const findCarById = require('../problem1');
const inventory = require('../inventory');

function testFindCarById() {
    const carId = 33;
    const  car = findCarById(inventory, carId);

    if (car){
        console.log(`Car ${carId} is a ${car.car_year} ${car.car_make} ${car.car_model}`);

    }
}


//running the test
testFindCarById();