function filterBMWandAUDI(inventory){
    const BMWandAUDI = [];
    for (let i=0; i<inventory.length; i++){
        const car  = inventory[i];
        if (car.car_make === "BMW" || car.car_make === "Audi"){
            BMWandAUDI.push(car);
        }
    }
    return BMWandAUDI;
}

module.exports = filterBMWandAUDI;