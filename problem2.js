function getLastCarInfo(inventory){
    if (inventory.length === 0){
        return "Inventory is empty";
    }else{
        const lastCar = inventory[inventory.length-1];
        return `Last car is a ${lastCar.car_year} ${lastCar.car_make} ${lastCar.car_model}`;
    }
}
module.exports = getLastCarInfo;