function carYears(inventory){
    const yearList = [] 
    
    for (let i=0; i<inventory.length; i++){
        yearList.push(inventory[i].car_year);
    }
    return yearList;
}

module.exports = carYears;
